﻿/*
 PROG 2070 Assignment 1
 Simple geometry application used for unit testing
 Revision History:
        John Lane, 2019.01.30: Created
 */
using System;

namespace John_Lane_A1_Prog2070
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle circle = new Circle();

            bool isEnteringRadius = true; //puts set radius menu on a loop

            while (isEnteringRadius)
            {
                Console.Write("Please enter the circle's radius: ");

                try // catches user input errors
                {
                    int radius = int.Parse(Console.ReadLine());

                    if (radius > 0)
                    {
                        circle = new Circle(radius);
                        isEnteringRadius = false;
                    }
                    else
                    {
                        Console.WriteLine("Radius has to be greater than 0");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message); //ouputs exception specific error
                }
            }

            bool isUsingMenu = true; // put menu on a loop

            while (isUsingMenu) 
            {
                Console.WriteLine("Enter 1 to get the circle's radius.");
                Console.WriteLine("Enter 2 to change the circle's radius.");
                Console.WriteLine("Enter 3 to get the circle's circumference.");
                Console.WriteLine("Enter 4 to get the circle's area.");
                Console.WriteLine("Enter 5 to exit.");
                Console.Write("Selection: ");

                try // catch user input errors on in menu
                {
                    byte userChoice = byte.Parse(Console.ReadLine());
                    
                    switch (userChoice)
                    {
                        case 1:
                            Console.WriteLine("Radius: " + circle.GetRadius());
                            break;
                        case 2:
                            Console.Write("Please enter a new radius: ");
                            try
                            {
                                int newRadius = int.Parse(Console.ReadLine());
                                if (newRadius > 0)
                                {
                                    circle.SetRadius(newRadius);
                                }
                                else
                                {
                                    Console.WriteLine("The radius can't" +
                                        " be less than 0.");
                                }
                            }
                            catch (Exception e)
                            {

                                Console.WriteLine(e.Message);
                            }

                            break;
                        case 3:
                            Console.WriteLine("Circumference: " + 
                                circle.GetCircumference());
                            break;
                        case 4:
                            Console.WriteLine("Area: " + circle.GetArea());
                            break;
                        case 5:
                            isUsingMenu = false;
                            break;
                        default:
                            Console.WriteLine("Please choose an " +
                                "option from 1-5.");
                            break;
                    }
                }
                
                catch (Exception e)
                {
                    Console.WriteLine(e.Message); // outputs exception specific error message
                }

            }

        }
    }
}

