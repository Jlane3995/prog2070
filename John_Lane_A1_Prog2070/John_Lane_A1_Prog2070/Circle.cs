﻿/*
 PROG 2070 Assignment 1
 Simple geometry application used for unit testing
 Revision History:
        John Lane, 2019.01.30: Created
 */

using System;

namespace John_Lane_A1_Prog2070
{

    public class Circle
    {
        private int radius;

        public Circle()
        {
            radius = 1;
        }

        public Circle(int radius)
        {
            this.radius = radius;
        }
        /// <summary>
        /// fetches the circle's radius
        /// </summary>
        /// <returns></returns>
        public int GetRadius()
        {
            return radius;
        }
        /// <summary>
        /// Lets user set the circle's input
        /// </summary>
        /// <param name="radius"></param>
        public void SetRadius(int radius)
        {
            this.radius = radius;
            Console.WriteLine("The new radius is: " + " " + radius);
        }
        /// <summary>
        /// Fetches the circle's circumference
        /// </summary>
        /// <returns></returns>
        public double GetCircumference()
        {
            double circumference = 2 * Math.PI * radius;

            return circumference;
        }
        /// <summary>
        /// Fetches the circle's area.
        /// </summary>
        /// <returns></returns>
        public double GetArea()
        {
            double area = Math.PI * Math.Pow(radius, 2);
            return area;

        }


    }
}



