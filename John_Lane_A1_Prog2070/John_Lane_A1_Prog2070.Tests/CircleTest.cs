﻿/*
 PROG 2070 Assignment 1
 Simple geometry application used for unit testing
 Revision History:
        John Lane, 2020.01.30: Created
 */

using NUnit.Framework;
using System;

namespace John_Lane_A1_Prog2070.Tests
{
    [TestFixture]
    public class CircleTest
    {
        [Test]
        public void GetRadius_DoesNotReturnZero()
        {
            Circle circle = new Circle();

            int radiusValue = circle.GetRadius();

            Assert.NotZero(radiusValue);
        }

        [Test]
        public void SetRadius_RadiusChanged() 
        {
            var circle = new Circle();

            int defaultRadius = circle.GetRadius();

            circle.SetRadius(2);

            int changedRadius = circle.GetRadius();

            Assert.AreNotEqual(defaultRadius, changedRadius);
        
        }

        [Test]
        public void GetCircumference_ReturnsCorrectValue() 
        {
            var circle = new Circle(3);

            var comparisonCircumference = 2 * Math.PI * 3;

            Assert.AreEqual(circle.GetCircumference(), comparisonCircumference);
        }

        [Test]
        public void GetArea_ReturnsCorrectValue() 
        {
            var circle = new Circle(3);

            var comparisonArea = Math.PI * Math.Pow(3, 2);

            Assert.AreEqual(circle.GetArea(), comparisonArea);

        }
    }
}
